# Test
# CSS Tips:

Current:

```
.dashboard-programs {
    ...

    .dashboard-programs-sort {
        ...

        .dashboard-programs-sort-label {
            ...
        }
    }
}
```

CSS:

```
.dashboard-programs { ... }
.dashboard-programs .dashboard-programs-sort { ... }
.dashboard-programs .dashboard-programs-sort .dashboard-programs-sort-label { ... }
```

- - -

BEM & name-nesting:

```
.dashboard-programs {
    ...

    &__sort {
        ...

        &-label { // not &__label
            ...
        }
    }
}
```

CSS:

```
.dashboard-programs { ... }
.dashboard-programs__sort { ... }
.dashboard-programs__sort-label { ... }
```



## One level nesting (only if necessary):

```
.dashboard-programs {
    ...

    & &__sort {
        ...
    }
}
```

CSS:

```
.dashboard-programs { ... }
.dashboard-programs .dashboard-programs__sort { ... }
```

## Deep nesting (only if it is really necessary):

```
.dashboard-programs {
    ...

    $parent: &;

    &__sort {
        ...

        #{$parent} &-label {
            ...
        }
    }
}
```

CSS:

```
.dashboard-programs { ... }
.dashboard-programs__sort { ... }

.dashboard-programs .dashboard-programs__sort-label { ... }
```
